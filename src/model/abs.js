/**
 * Abs
 */
const postgres = require('../helper/postgres');

class AbstractModel {
    constructor() {
        this.postgres = postgres;
    }

    static initial() {
        throw new Error('Method is not implement!');
    }

    static drop() {
        throw new Error('Method is not implement!');
    }
}

module.exports = AbstractModel;