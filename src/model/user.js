/**
 * User Model
 */
const Abs = require('./abs');
const tableName = 'users';
const moment = require('moment')

class UserModel extends Abs {
    constructor() {
        super();
    }

    async create(username, password, email, fullname, address) {
        return await this.postgres.insert({
            username: username,
            password: password,
            email: email,
            fullname: fullname,
            address: address,
            created_at: moment().utc().format(),
        }).into(tableName).returning("*").then((resp) => {
            return {
                success: 1,
                data: resp[0],
                table: tableName
            }
        }).catch((err) => {
            return {
                status: 201,
                message: 'Create Error',
                detail: err.detail,
            };
        });
        // return user;
    }

    async findByUsername(username) {
        return this.postgres.select("username").from('login').where('username', '=', username);
    }

    async findByUserId(id) {
        return this.postgres.select('*').from(tableName).where(
            "id", "=", id
        ).returning(
            "fullname",
            "email"
        )
    }

    async deleteByUsername(username) {
        return this.postgres.from(tableName).del().where("username", '=', username).returning("*")
    }

    async deleteByUserId(id) {
        return this.postgres.from(tableName).del().where("id", '=', id).returning("*")
    }

    async findAllUsers() {
        return this.postgres.select("*").from(tableName).returning(
            "username",
            "email",
            "fullname",
            'address'
        )
    }

    async updateUserById(id, data) {
        return this.postgres.select("*").from(tableName).where({ id: id }).returning("*").update({
            username: data.username,
            password: data.password,
            email: data.email,
            address: data.address,
            fullname: data.fullname
        }).then(resp => {
            if (resp.length >= 1) {
                return {
                    success: 1,
                    message: "Update Success",
                    data: resp[0],
                }
            } else {
                return {
                    success: 0,
                    message: "Cannot find user"
                }
            }
        }).catch(err => {
            return {
                success: 0,
                message: "Cannot Update",
                data: err
            }
        })
    }

    static initial(knex) {
        return knex.schema.createTable(tableName, (table) => {
            table.increments('id').unique().notNullable();
            table.text('username').unique().notNullable();
            table.text('password').notNullable();
            table.text('fullname').notNullable();
            table.text('email').unique().notNullable();
            table.text('address').notNullable();
            table.datetime('created_at');
        }).then().catch();
    }

    static drop(knex) {
        return knex.schema.dropTableIfExists(tableName).then().catch();
    }
}

module.exports = UserModel;