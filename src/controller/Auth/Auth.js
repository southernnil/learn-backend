const LoginModel = require('../../model/login');

const Login = new LoginModel(); // create model

var Auth = {

    ping: async function(username, email) {
        return await Login.findByUsername(username)
            .then((user) => {
                if (user.length > 0) {
                    return true;
                } else {
                    return false;
                }
            })
            .catch((err) => {
                return err
            });
    },

    login: async function(body) {
        return await Login.login(body)
    },

    findUserLoggedIn: async function(username) {
        return await Login.findByUsername(username)
    },

    logout: async function(username) {
        return await Login.logout(username)
    },

    forgotPassword: async function(email) {
        let findEmail = await Login.forgotPassword(email)
        if (findEmail.length >= 1) {
            return {
                success: 1,
                status: true,
                message: `${email} exist in the system `
            }
        } else {
            return {
                success: 0,
                status: false,
                message: `${email} does not exist in the system`
            }
        }
    }

}

module.exports = Auth