/**
 * Controller
 */


const User = require('./Users/User')

const Auth = require('./Auth/Auth')

module.exports = {
    Auth,
    User
};