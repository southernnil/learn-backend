const koa = require('koa')
const Router = require('koa-router')
const bodyParser = require('koa-bodyparser')
const cors = require("@koa/cors")
const jwt = require('jsonwebtoken')

// Import Router
const UserRouter = require("./routes/users")

const tokenUser = ""

const app = new koa()
const router = new Router()
app.use(cors())
app.use(bodyParser())

// Ping Status token

const secretKey = "c#@!123"
router.get('/ping', async(ctx, next) => {
    const isVerify = await jwt.verify(tokenUser, secretKey, {
        complete: false,
        ignoreExpiration: true,
    }, (err, token) => {
        if (err) {
            return err
        } else {
            console.log(token)
            return token
        }
    })
    await ctx.response.toJSON({
        verify: isVerify
    })

})

// Use Router User
app.use(UserRouter)

app.use(router.routes());
app.use(router.allowedMethods());

const port = process.env.PORT || 8080
app.listen(port, () => {
    console.log(`Server is running on port ${port}`)
})