const bcrypt = require("bcrypt-nodejs")

const salt = bcrypt.genSaltSync(10)

const passwordCrypt = (password) => {
    return bcrypt.hashSync(password, salt)
}
const comparePassword = (password, saltPassword) => {
    return bcrypt.compareSync(password, saltPassword)
}
module.exports = { passwordCrypt, comparePassword }