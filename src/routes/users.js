const Router = require('koa-router');
var UserRouter = new Router();

const { Auth, User } = require('../controller');

/* GET users listing. */
UserRouter.get('/', function (ctx, next) {
    ctx.response.body('respond with a resource');
});

// Sigin and Register
UserRouter.post('/register', async (ctx, next) => {
    let body = ctx.request.body;

    let result = await User.register(body);
    ctx.response.body = result;
});

UserRouter.post('/login', async (ctx, next) => {
    const body = ctx.request.body;
    let result = await Auth.login(body);
    ctx.response.body = result;
    next();
});

// Sign Out
UserRouter.post('/signout/:username', async (ctx, next) => {
    let username = ctx.params.username;
    let handleSignout = await Auth.logout(username);
    if (handleSignout.success !== 0) {
        ctx.response.body = {
            success: 1,
            message: `${handleSignout.data.username} has signout`,
        };
    } else {
        ctx.response.body = {
            success: 0,
            message: 'Sign out not success',
        };
    }
});

// Get profile user
UserRouter.get('/profile/:username', async (ctx, next) => {
    const username = ctx.params.username;
    let infoUser = await User.getInfoUserById(username)
    ctx.response.body = infoUser;
});

// Update User Profile
UserRouter.put('/update/:id', async (ctx, nex) => {
    const id = ctx.params.id;

    const body = ctx.request.body;
    const findInfoUsername = await User.updateByUserId(id, body)
    ctx.response.body = findInfoUsername;
});

// Delete user by id
UserRouter.delete('/delete/:id', async (ctx, next) => {
    const { id } = ctx.request.params;
    let result = await DeleteUser(id);
    ctx.response.body = result;
});

// Forgot Password
UserRouter.get("/forgot-password", async (ctx, next) => {
    const body = ctx.request.body
    let getEmail = await Auth.forgotPassword(body.email)
    ctx.response.body = getEmail
})

// Get all user
UserRouter.get('/all-user', async (ctx, next) => {
    let allUser = await User.getAllUsers()
    next();
    ctx.response.body = allUser;

})

module.exports = UserRouter.middleware()