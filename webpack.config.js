const path = require('path')
const nodeExternals = require("webpack-node-externals")

module.exports = {
  target: "node",
  entry: {
    server: ['./src/index.js']
  },
  output: {
    path: path.resolve(__dirname, "./build"),
    filename: "[name].js"
  },
  node: {
    fs: 'empty'
  },
  externals: [nodeExternals()]
}
